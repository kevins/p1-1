<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TPublication extends Model
{
	protected $table='tpublication';
	protected $primaryKey='idPublication';
	public $incrementing=true;
	public $timestamps=false;
}
?>