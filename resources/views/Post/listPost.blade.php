@extends('shared.template')
@section('dashboardUser')
<style>
	.col-lg-12
	{
		padding: 0;
	}
	.firstColum{margin-top: 25%;}
	.secondColum{margin-top: 10%;}
	.thirdColum{}
	.fourColum{margin-top: 10%;}
</style>
<section class="archive_area section-gap">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="main_title">
					<h1>Let Us Find Your Places Within a Sec.</h1>
					<p>
						There is a moment in the life of any aspiring astronomer that it is time to buy that first telescope. It’s
						exciting to think
						about setting up your own viewing station.
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-lg-3 firstColum">
				<div class="col-lg-12 col-md-3 col-sm-12">
					<div class="single_travel wow fadeInUp" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: fadeInUp;">
						<figure>
							<img class="img-fluid w-100" src="{{asset('img/category/c5.jpg')}}" alt="">
						</figure>
						<div class="overlay"></div>
						<div class="text-wrap">
							<h3>
								<a href="#">Waterfall Travel</a>
							</h3>
							<div class="blog-meta white d-flex justify-content-between align-items-center flex-wrap">
								<div class="meta">
									<a href="#">
										<span class="icon fa fa-calendar"></span> March 14, 2018
										<span class="icon fa fa-comments"></span> 05
									</a>
								</div>
								<div>
									<a class="read_more" href="#">Read More</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-12 col-md-3 col-sm-12">
					<div class="single_travel wow fadeInUp" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: fadeInUp;">
						<figure>
							<img class="img-fluid w-100" src="{{asset('img/category/c2.jpg')}}" alt="">
						</figure>
						<div class="overlay"></div>
						<div class="text-wrap">
							<h3>
								<a href="#">Waterfall Travel</a>
							</h3>
							<div class="blog-meta white d-flex justify-content-between align-items-center flex-wrap">
								<div class="meta">
									<a href="#">
										<span class="icon fa fa-calendar"></span> March 14, 2018
										<span class="icon fa fa-comments"></span> 05
									</a>
								</div>
								<div>
									<a class="read_more" href="#">Read More</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 secondColum">
				<div class="col-lg-12 col-md-3 col-sm-12">
					<div class="single_travel wow fadeInUp" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: fadeInUp;">
						<figure>
							<img class="img-fluid w-100" src="{{asset('img/category/c3.jpg')}}" alt="">
						</figure>
						<div class="overlay"></div>
						<div class="text-wrap">
							<h3>
								<a href="#">Waterfall Travel</a>
							</h3>
							<div class="blog-meta white d-flex justify-content-between align-items-center flex-wrap">
								<div class="meta">
									<a href="#">
										<span class="icon fa fa-calendar"></span> March 14, 2018
										<span class="icon fa fa-comments"></span> 05
									</a>
								</div>
								<div>
									<a class="read_more" href="#">Read More</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-12 col-md-3 col-sm-12">
					<div class="single_travel wow fadeInUp" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: fadeInUp;">
						<figure>
							<img class="img-fluid w-100" src="{{asset('img/category/c5.jpg')}}" alt="">
						</figure>
						<div class="overlay"></div>
						<div class="text-wrap">
							<h3>
								<a href="#">Waterfall Travel</a>
							</h3>
							<div class="blog-meta white d-flex justify-content-between align-items-center flex-wrap">
								<div class="meta">
									<a href="#">
										<span class="icon fa fa-calendar"></span> March 14, 2018
										<span class="icon fa fa-comments"></span> 05
									</a>
								</div>
								<div>
									<a class="read_more" href="#">Read More</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 thirdColum">
				<div class="col-lg-12 col-md-3 col-sm-12">
					<div class="single_travel wow fadeInUp" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: fadeInUp;">
						<figure>
							<img class="img-fluid w-100" src="{{asset('img/category/c5.jpg')}}" alt="">
						</figure>
						<div class="overlay"></div>
						<div class="text-wrap">
							<h3>
								<a href="#">Waterfall Travel</a>
							</h3>
							<div class="blog-meta white d-flex justify-content-between align-items-center flex-wrap">
								<div class="meta">
									<a href="#">
										<span class="icon fa fa-calendar"></span> March 14, 2018
										<span class="icon fa fa-comments"></span> 05
									</a>
								</div>
								<div>
									<a class="read_more" href="#">Read More</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-12 col-md-3 col-sm-12">
					<div class="single_travel wow fadeInUp" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: fadeInUp;">
						<figure>
							<img class="img-fluid w-100" src="{{asset('img/category/c6.jpg')}}" alt="">
						</figure>
						<div class="overlay"></div>
						<div class="text-wrap">
							<h3>
								<a href="#">Waterfall Travel</a>
							</h3>
							<div class="blog-meta white d-flex justify-content-between align-items-center flex-wrap">
								<div class="meta">
									<a href="#">
										<span class="icon fa fa-calendar"></span> March 14, 2018
										<span class="icon fa fa-comments"></span> 05
									</a>
								</div>
								<div>
									<a class="read_more" href="#">Read More</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 fourColum">
				<div class="col-lg-12 col-md-3 col-sm-12">
					<div class="single_travel wow fadeInUp" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: fadeInUp;">
						<figure>
							<img class="img-fluid w-100" src="{{asset('img/category/c3.jpg')}}" alt="">
						</figure>
						<div class="overlay"></div>
						<div class="text-wrap">
							<h3>
								<a href="#">Waterfall Travel</a>
							</h3>
							<div class="blog-meta white d-flex justify-content-between align-items-center flex-wrap">
								<div class="meta">
									<a href="#">
										<span class="icon fa fa-calendar"></span> March 14, 2018
										<span class="icon fa fa-comments"></span> 05
									</a>
								</div>
								<div>
									<a class="read_more" href="#">Read More</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-12 col-md-3 col-sm-12">
					<div class="single_travel wow fadeInUp" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: fadeInUp;">
						<figure>
							<img class="img-fluid w-100" src="{{asset('img/category/c2.jpg')}}" alt="">
						</figure>
						<div class="overlay"></div>
						<div class="text-wrap">
							<h3>
								<a href="#">Waterfall Travel</a>
							</h3>
							<div class="blog-meta white d-flex justify-content-between align-items-center flex-wrap">
								<div class="meta">
									<a href="#">
										<span class="icon fa fa-calendar"></span> March 14, 2018
										<span class="icon fa fa-comments"></span> 05
									</a>
								</div>
								<div>
									<a class="read_more" href="#">Read More</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-12">
						<nav class="blog-pagination justify-content-center d-flex">
							<ul class="pagination">
								<li class="page-item">
									<a href="#" class="page-link" aria-label="Previous">
										<span aria-hidden="true">
											<span class="lnr lnr-chevron-left"></span>
										</span>
									</a>
								</li>
								<li class="page-item"><a href="#" class="page-link">01</a></li>
								<li class="page-item active"><a href="#" class="page-link">02</a></li>
								<li class="page-item"><a href="#" class="page-link">03</a></li>
								<li class="page-item"><a href="#" class="page-link">04</a></li>
								<li class="page-item"><a href="#" class="page-link">09</a></li>
								<li class="page-item">
									<a href="#" class="page-link" aria-label="Next">
										<span aria-hidden="true">
											<span class="lnr lnr-chevron-right"></span>
										</span>
									</a>
								</li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection